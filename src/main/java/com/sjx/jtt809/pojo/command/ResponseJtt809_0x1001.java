package com.sjx.jtt809.pojo.command;

import com.sjx.jtt809.pojo.Response;
import io.netty.buffer.ByteBuf;

import java.nio.charset.Charset;

/**
 * 主链路登录请求消息
 * 链路类型:主链路。
 * 消息方向:下级平台往上级平台。
 * 业务数据类型标识: UP-CONNECT-REQ。
 * 描述:下级平台向上级平台发送用户名和密码等登录信息。
 */
public class ResponseJtt809_0x1001 extends Response {

    /**
     * 用户名
     */
    private long userId;

    /**
     * 密码
     */
    private String password;

    /**
     * 下级平台提供对应的从链路服务端 IP 地址
     */
    private String downLinkIp;

    /**
     * 下级平台提供对应的从链路服务器端口号
     */
    private int downLinkPort;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDownLinkIp() {
        return downLinkIp;
    }

    public void setDownLinkIp(String downLinkIp) {
        this.downLinkIp = downLinkIp;
    }

    public int getDownLinkPort() {
        return downLinkPort;
    }

    public void setDownLinkPort(int downLinkPort) {
        this.downLinkPort = downLinkPort;
    }

    @Override
    protected void decodeImpl(ByteBuf buf) {
        this.userId = buf.readUnsignedInt();
        this.password = (buf.readBytes(8).toString(Charset.forName("GBK"))).trim();
        this.downLinkIp = (buf.readBytes(32).toString(Charset.forName("GBK"))).trim();
        this.downLinkPort = buf.readUnsignedShort();
    }

}
