package com.sjx.jtt809.handler.initializer;

import com.sjx.jtt809.codec.DecoderJtt809;
import com.sjx.jtt809.codec.EncoderJtt809;
import com.sjx.jtt809.handler.PrimaryLinkServerJtt809Handler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.timeout.IdleStateHandler;

import java.util.concurrent.TimeUnit;

public class PrimaryLinkServerJtt809Initializer extends ChannelInitializer<SocketChannel> {

    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ChannelPipeline channelPipeline = ch.pipeline();

        // 打印日志信息
//        channelPipeline.addLast("loging", new LoggingHandler(LogLevel.INFO));

        // 解码 和 编码
        channelPipeline.addLast("decoder", new DecoderJtt809());
        channelPipeline.addLast("encoder", new EncoderJtt809());

        // 心跳检测，每隔150s检测一次是否要读事件，如果超过150s你没有读事件的发生，则执行相应的操作
        channelPipeline.addLast("timeout", new IdleStateHandler(150, 0, 0, TimeUnit.SECONDS));

        // 业务逻辑Handler
        channelPipeline.addLast("handler", new PrimaryLinkServerJtt809Handler());
    }

}
