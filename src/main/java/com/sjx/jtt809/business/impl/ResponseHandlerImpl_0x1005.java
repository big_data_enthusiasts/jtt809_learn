package com.sjx.jtt809.business.impl;

import com.sjx.jtt809.pojo.command.RequestJtt809_0x1006;
import com.sjx.jtt809.pojo.command.ResponseJtt809_0x1005;
import com.sjx.jtt809.business.IBusinessServer;
import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;
import io.netty.channel.ChannelHandlerContext;

/**
 * 主链路连接保持请求消息
 * 链路类型:主链路。
 * 消息方向:下级平台往上级平台。
 * 业务数据类型标识:UP_ LINK 下 EST_ REQ。
 * 描述:下级平台向上级平台发送主链路连接保持清求消息，以保持主链路的连接。
 * 主链路连接保持清求消息，数据体为空。
 */
public class ResponseHandlerImpl_0x1005 implements IBusinessServer<ResponseJtt809_0x1005> {

    private static final Log logger = LogFactory.get();

    public void businessHandler(ChannelHandlerContext ctx, ResponseJtt809_0x1005 msg) {
        ctx.writeAndFlush(new RequestJtt809_0x1006());
    }
}
